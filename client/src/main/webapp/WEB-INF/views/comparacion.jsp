<%@ include file="/WEB-INF/views/includes.jsp"%>

<c:url var="loginPostUrl" value="/j_spring_security_check"/>
<c:url var="logoutUrl" value="/j_spring_security_logout"/>
<c:url var="loginUrl" value="/login"/>
<c:url var="contextUrl" value="/"/>

<html>

<head>
    <title>Comparaci�n</title>

    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <meta charset="UTF-8">

    <link rel="stylesheet" href="resources/css/bootstrap.css"/>
    <link rel="stylesheet" href="resources/css/font-awesome.css"/>
    <link rel="stylesheet" href="resources/css/basic.css"/>
    <link rel="stylesheet" href="resources/css/custom.css"/>
    <link rel="stylesheet" href="resources/css/styles.css">
    <link rel="stylesheet" href="resources/css/morris.css">
    <link rel="stylesheet" href="resources/css/prettify.min.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans"/>

    <link rel="stylesheet" media="screen" href="resources/css/ui.jqgrid-bootstarp.css"/>
    <link rel="stylesheet" media="screen" href="resources/css/ui.jqgrid.css"/>
    <link rel="stylesheet" media="screen" href="resources/css/jquery.ui.base.css"/>
    <link rel="stylesheet" media="screen" href="resources/css/jquery-ui.css"/>
</head>

<body class="body-custom">
<div class="container-fluid">

    <!-- BARRA DE NAVEGACION -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="col-md-12">
                <div class="navbar-collapse collapse" id="additional-nav">
                    <sec:authorize access="isAnonymous()">
                        <ul class="nav navbar-nav">
                            <li class="active" id="login"><a href="${loginUrl}">Login</a></li>
                        </ul>
                    </sec:authorize>
                    <sec:authorize access="isAuthenticated()">
                        <ul class="nav navbar-nav">
                            <li id="home"><a href="${contextUrl}home">Home</a></li>
                            <li id="amazonEC2"><a href="${contextUrl}amazon">Amazon EC2</a></li>
                            <li id="googleComputeEngine"><a href="${contextUrl}google">Google Compute Engine</a></li>
                            <li id="heroku"><a href="${contextUrl}heroku">Heroku</a></li>
                            <li id="comparacion" class="active"><a href="${contextUrl}comparacion">Comparaci�n</a></li>
                        </ul>
                    </sec:authorize>
                    <sec:authorize access="isAuthenticated()">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a class="dropdown-toggle" href="${logoutUrl}"><i class="fa fa-user fa-1.5x"></i>Logout</a></li>
                        </ul>
                    </sec:authorize>
                </div>
            </div>
        </div>
    </nav>

</div>

<!-- RUTA ACTUAL -->
<ol class="breadcrumb">
    <li class="active">Home</li>
    <li class="active">${pagina}</li>
</ol>

    <div class="row">
        <div class="col-md-12">
            <h3 class="page-head-line">
                <label id="tipoComparacion">Comparaci�n de Resultados</label>
            </h3>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-table fa-fw"></i> Tabla Comparativa
                    <div class="pull-right">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown"
                                style="height: 25px; margin-top: -1px; width: 100px;">
                                    Acciones
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a href="javascript:resultadoComparacion('promedio');">Promedio</a></li>
                                    <li><a href="javascript:resultadoComparacion('ultima');">�ltima Ejecuci�n</a></li>
                                </ul>
                            </div>
                        </div>
                </div>
                <div class="panel-body" align="center" style="border-style:solid; color: #F5F5F5; border-width: 1px;">
                    <table id="comparacionGrid" style="font-family: 'Open Sans', sans-serif;"></table>
                </div>
            </div>

            <div>
                <!-- DONUT CHART -->
                <div class="col-lg-4">
                    <div class="panel panel-default"
                        style="margin-left: -15px; margin-bottom: -16px; height: 512px; border-bottom-right-radius: 0px;">
                        <div class="panel-heading" style="border-top-right-radius: 0px;">
                            <i class="fa fa-pie-chart fa-fw"></i> Gr�fico por Atributo
                        </div>
                        <div align="center">
                            <br/>
                            <div class="form-group">
                                <label class="col-xs-3 control-label" style="left: 20px; top: -1px;">Atributos:</label>
                                <div class="col-xs-9 selectContainer">
                                    <select id="combobox" class="form-control" style="margin-left:-15px; height: 25px; width: 200px;">
                                        <option value="0">Seleccione...</option>
                                        <option value="1">Ancho de banda</option>
                                        <option value="2">CPU</option>
                                        <option value="3">Escritura en Disco</option>
                                        <option value="4">Escritura en Memoria</option>
                                        <option value="5">Procesamiento</option>
                                        <option value="6">Latencia</option>
                                        <option value="7">Lectura en Disco</option>
                                        <option value="8">Lectura en Memoria</option>
                                    </select>
                                </div>
                            </div>
                            <br/><br/><br/>
                            <div id="morris-donut-chart" style="width:200px; height:200px;"></div>
                            <br/>
                            <div id="mejorResultado" class="list-group-item" style="display: none; width: 250px; height: 40px;">
                                <div style="margin-top: -5px">
                                    <label>Mejor tiempo:</label>
                                    <span id="mejorTiempo"></span> [<span id="mejorAtributo"></span>]
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- BAR CHART -->
            <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bar-chart-o fa-fw"></i> Gr�fico de Atributos vs Tiempo [seg.].
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-8" align="center">
                                <br/>
                                <div id="morris-bar-chart"></div>
                                <br/>
                                <!-- LEYENDA -->
                                <table>
                                    <tr>
                                        <td>
                                            <div class="list-group-item" style="width: 350px; height: 35px">
                                                <table style="margin-top: -4px;">
                                                    <tr>
                                                        <td>
                                                            <div id="leyenda-amazon"></div>
                                                        </td>
                                                        <td>
                                                            <span style="margin-left: 10px">Amazon</span>
                                                        </td>
                                                        <td>
                                                            <div id="leyenda-google" style="margin-left: 35px"></div>
                                                        </td>
                                                        <td>
                                                            <span style="margin-left: 10px">Google</span>
                                                        </td>
                                                        <td>
                                                            <div id="leyenda-heroku" style="margin-left: 35px"></div>
                                                        </td>
                                                        <td>
                                                            <span style="margin-left: 10px">Heroku</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

        <h3 class="page-subhead-line"></h3>

    </div>

    <div style="display:none;">
        <input id="hostAddress" type="text" value="${hostAddress}">
        <input id="globalTipoComparacion" type="text" value="">
    </div>

    <div id="footer-sec">
        &copy; 2016 Prototipo | Design by : <a href="https://www.linkedin.com/in/juan-carlos-calvopi�a-m-49544272" target="_blank">Juan Calvopi�a Morillo</a> | <a href="mailto:juan.calvopina@gmail.com?Subject=Prototype-Model-Project" target="_top" target="_blank"> e-mail</a>
    </div>

</body>

<script src="<c:url value="/resources/js/jquery-1.10.2.js"/>"></script>
<script src="<c:url value="/resources/js/raphael-min.js"/>"></script>
<script src="<c:url value="/resources/js/morris.min.js"/>"></script>
<script src="<c:url value="/resources/js/bootstrap.js"/>"></script>

<script src="<c:url value="/resources/js/lib/jquery.min.js"/>"></script>
<script src="<c:url value="/resources/js/lib/jquery-ui.min.js"/>"></script>
<script src="<c:url value="/resources/js/lib/jquery.jqGrid.min.js"/>"></script>
<script src="<c:url value="/resources/js/lib/i18n/grid.locale-en.js"/>"></script>

<script src="<c:url value="/resources/js/app/morris-bar-data.js"/>"></script>
<script src="<c:url value="/resources/js/app/morris-donut-data.js"/>"></script>
<script src="<c:url value="/resources/js/app/tablaComparativa.js"/>" charset="UTF-8"></script>

</html>