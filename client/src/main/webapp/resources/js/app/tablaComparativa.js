//obtiene el contexto de la aplicacion
var contextPathUrl = window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));

// obtiene la ip del servidor cliente
var hostAddress = $("#hostAddress").val();

// setea el puerto
var portNumber = 80;
if (hostAddress == "localhost")
  portNumber = 8080;

// url para obtener los resultados de la comparacion
var jsonUrlComparacion = "http://" + hostAddress + ":" + portNumber + contextPathUrl;

$(document).ready(function() {
  $("#combobox").prop("disabled", true);
});

function resultadoComparacion(tipo) {

  $("#globalTipoComparacion").val(tipo);

  $("#combobox").prop("disabled", false);

  var jsonComparacion = ""
  if (tipo == "promedio") {
    $("#tipoComparacion").html("Comparación de Resultados - Promedio");
    jsonComparacion = jsonUrlComparacion + "/getPromedioComparacion.json";
  } else {
    $("#tipoComparacion").html("Comparación de Resultados - Última Ejecución");
    jsonComparacion = jsonUrlComparacion + "/getComparacion.json";
  }

  var grid = $("#comparacionGrid"), headerRow, rowHight, resizeSpanHeight;

  grid.jqGrid({
    url : jsonComparacion,
    datatype : 'json',
    headertitles : true,
    jsonReader : {
      repeatitems : false,
      root : function(obj) {
        if ($.isArray(obj)) {
          return obj;
        }
        if ($.isArray(obj.items)) {
          return obj.items;
        }
        return [];
      },
      page : function() {
        return 1;
      },
      total : function() {
        return 1;
      },
      records : function(obj) {
        if ($.isArray(obj)) {
          return obj.length;
        }
        if ($.isArray(obj.items)) {
          return obj.items.length;
        }
        return 0;
      }
    },
    colNames : [ 'Servidor', 'Ancho de Banda - 2 MB / [seg.]', 'CPU [seg.]', 'Escritura en Disco - 5 MB / [seg.]',
        'Escritura en Memoria - 5 MB / [seg.]', 'Procesamiento [seg.]', 'Latencia [seg.]',
        'Lectura en Disco - 5 MB / [seg.]', 'Lectura en Memoria - 5 MB / [seg.]', 'Fecha' ],
    colModel : [ {
      name : 'servidor',
      index : 'servidor',
      width : 70,
      sortable : false
    }, {
      name : 'anchoBanda',
      index : 'anchoBanda',
      width : 115,
      sorttype : 'float',
      align : "right"
    }, {
      name : 'cpu',
      index : 'cpu',
      width : 70,
      sorttype : 'float',
      align : "right"
    }, {
      name : 'escrituraDisco',
      index : 'escrituraDisco',
      width : 130,
      sorttype : 'float',
      align : "right"
    }, {
      name : 'escrituraMemoria',
      index : 'escrituraMemoria',
      width : 150,
      sorttype : 'float',
      align : "right"
    }, {
      name : 'instruccionesMinuto',
      index : 'instruccionesMinuto',
      width : 105,
      sorttype : 'float',
      align : "right"
    }, {
      name : 'latencia',
      index : 'latencia',
      width : 70,
      sorttype : 'float',
      align : "right"
    }, {
      name : 'lecturaDisco',
      index : 'lecturaDisco',
      width : 120,
      sorttype : 'float',
      align : "right"
    }, {
      name : 'lecturaMemoria',
      index : 'lecturaMemoria',
      width : 140,
      sorttype : 'float',
      align : "right"
    }, {
      name : 'fecha',
      index : 'fecha',
      width : 90,
      sorttype : 'date',
      align : "center",
      autowidth : true,
    } ],
    height : 106,
    width : '100%',
    scrollOffset : 1,
    sortname : 'fecha',
    viewrecords : true,
    gridview : true,
    emptyrecords : "No hay registros",
    loadComplete : function() {
      loadMorrisBar();
      console.log("Tabla comparativa carga completa");
    },
    loadError : function(jqXHR, textStatus, errorThrown) {
      alert('Se produjo un error al cargar la tabla');
      console.log('Se produjo un error al cargar la tabla comparativa: HTTP status code: ' + jqXHR.status + '\n'
          + 'textstatus: ' + textStatus + '\n' + 'errorThrown: ' + errorThrown);
      console.log('HTTP message body  (jqXHR.responseText: ' + '\n' + jqXHR.responseText);
    }
  });

  // refrescar tabla
  $("#comparacionGrid").jqGrid('setGridParam', {
    datatype : 'json',
    url : jsonComparacion
  }).trigger('reloadGrid');

  // Obtiene la fila de la cabecera
  headerRow = grid.closest("div.ui-jqgrid-view").find("table.ui-jqgrid-htable>thead>tr.ui-jqgrid-labels");

  // Aumenta la altura
  resizeSpanHeight = 'height: ' + headerRow.height() + 'px !important; cursor: col-resize;';
  headerRow.find("span.ui-jqgrid-resize").each(function() {
    this.style.cssText = resizeSpanHeight;
  });

  // alinea el texto en el centro
  rowHight = headerRow.height();
  headerRow.find("div.ui-jqgrid-sortable").each(function() {
    var ts = $(this);
    ts.css('top', (rowHight - ts.outerHeight()) / 2 + 'px');
  });

  // tootip
  var setTooltipsOnColumnHeader = function(grid, iColumn, text) {
    var thd = jQuery("thead:first", grid[0].grid.hDiv)[0];
    jQuery("tr.ui-jqgrid-labels th:eq(" + iColumn + ")", thd).attr("title", text);
  };

  // agrega titulo a la cabecera
  setTooltipsOnColumnHeader($("#comparacionGrid"), 0, "Servidor de captura");
  setTooltipsOnColumnHeader($("#comparacionGrid"), 1, "Ancho de Banda - 2 MB / [seg.]");
  setTooltipsOnColumnHeader($("#comparacionGrid"), 2, "CPU - [seg.]");
  setTooltipsOnColumnHeader($("#comparacionGrid"), 3, "Escritura en Disco - 5 MB / [seg.]");
  setTooltipsOnColumnHeader($("#comparacionGrid"), 4, "Escritura en Memoria - 5 MB / [seg.]");
  setTooltipsOnColumnHeader($("#comparacionGrid"), 5, "Procesamiento - [seg.]");
  setTooltipsOnColumnHeader($("#comparacionGrid"), 6, "Latencia - [seg.]");
  setTooltipsOnColumnHeader($("#comparacionGrid"), 7, "Lectura en Disco - 5 MB / [seg.]");
  setTooltipsOnColumnHeader($("#comparacionGrid"), 8, "Lectura en Memoria - 5 MB / [seg.]");
  setTooltipsOnColumnHeader($("#comparacionGrid"), 9, "Fecha de captura");
}
