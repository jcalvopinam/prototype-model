//obtiene el contexto de la aplicacion
var contextPathUrl = window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));

// obtiene la ip del servidor cliente
var hostAddress = $("#hostAddress").val();

// setea el puerto
var portNumber = 80;
if (hostAddress == "localhost")
  portNumber = 8080;

// url para los resultados de la comparacion
var jsonUEBar = "http://" + hostAddress + ":" + portNumber + contextPathUrl;

var jsonBarComparacion = ""

// $(document).ready(loadMorrisBar());
var barJson = "";

function loadMorrisBar() {

  // limia en nodo "morris-bar-chart"
  var barChart = document.getElementById("morris-bar-chart");
  barChart.innerHTML = '';

  while (barChart.firstChild) {
    barChart.removeChild(barChart.firstChild);
  }

  var tipo = $("#globalTipoComparacion").val();

  if (tipo == "promedio") {
    jsonBarComparacion = jsonUEBar + "/getEjecucionPromedio";
  } else {
    jsonBarComparacion = jsonUEBar + "/getUltimaEjecucion";
  }

  barJson = $.getJSON(jsonBarComparacion, function(json) {

    $(function() {
      Morris.Bar({
        element : 'morris-bar-chart',
        data : json,
        parseTime : false,
        barColors : [ '#e8a02a', '#3974c4', '#413290' ],
        ymax : 'auto',
        ymin : 'auto',
        xkey : 'atributo',
        ykeys : [ 'amazon', 'google', 'heroku' ],
        labels : [ 'Amazon EC2', 'Google Compute Engine', 'Heroku' ],
        pointSize : 2,
        hideHover : 'auto',
        resize : true
      });

    });
  }).done(function() {
    $("#waiting").hide();
  }).fail(function() {
    $("#waiting").hide();
    alert("No se pudo recuperar datos de la URL ingresada");
  });

}

if (barJson != "") {
  barJson.complete(function() {
    console.log("Procesado con exito bar chart");
  });
}
