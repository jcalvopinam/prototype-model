//obtiene el contexto de la aplicacion
var contextPathUrl = window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));

// obtiene el nombre del servidor quien hace la peticion
var serverName = $("#serverName").val();

// obtiene la ip del servidor cliente
var hostAddress = $("#hostAddress").val();

// setea el puerto
var portNumber = 80;
if (hostAddress == "localhost")
  portNumber = 8080;

// url para obtener el historial de ejecuciones se envia como parametro el
// nombre del servidor
var jsonUrl = "http://" + hostAddress + ":" + portNumber + contextPathUrl + "/getHistorialEjecuciones/" + serverName;

$(document).ready(loadTablaResultados());

function loadTablaResultados() {

  $("#accordion").accordion({
    event : "click",
    active : 0,
    collapsible : true,
    autoHeight : false,
    height : 600
  });

  $("#dataGrid").jqGrid({
    url : jsonUrl,
    datatype : "json",
    styleUI : 'Bootstrap',
    headertitles : true,
    jsonReader : {
      repeatitems : false,
      root : function(obj) {
        return obj;
      }
    },
    colNames : [ 'id', 'CPU', 'LM', 'EM', 'LD', 'ED', 'AB', 'L', 'P', 'Fecha', 'Servidor' ],
    colModel : [ {
      name : 'id',
      index : 'id',
      key : true,
      width : 60,
      hidden : true
    }, {
      name : 'cpu',
      index : 'cpu',
      width : 60,
      align : "right"
    }, {
      name : 'lecturaMemoria',
      index : 'lecturaMemoria',
      width : 60,
      align : "right"
    }, {
      name : 'escrituraMemoria',
      index : 'escrituraMemoria',
      width : 60,
      align : "right"
    }, {
      name : 'lecturaDisco',
      index : 'lecturaDisco',
      width : 60,
      align : "right"
    }, {
      name : 'escrituraDisco',
      index : 'escrituraDisco',
      width : 60,
      align : "right"
    }, {
      name : 'anchoBanda',
      index : 'anchoBanda',
      width : 60,
      align : "right"
    }, {
      name : 'latencia',
      index : 'latencia',
      width : 60,
      align : "right"
    }, {
      name : 'instruccionesMinuto',
      index : 'instruccionesMinuto',
      width : 60,
      align : "right"
    }, {
      name : "fecha",
      index : 'fecha',
      width : 90,
      align : "center",
      shrinkToFit : false,
    }, {
      name : 'servidor',
      index : 'servidor',
      width : 290,
      hidden : true
    }, ],
    rowNum : 10,
    rowList : [ 10, 20, 30, 40, 50, 100, 500, 100000000 ],
    sortname : 'id',
    sortorder : "asc",
    pager : '#pagerGrid',
    height : 275,
    viewrecords : true,
    loadComplete : function() {
      $("option[value=100000000]").text('All');
      console.log("carga completa de resultados");
    }
  });

  var setTooltipsOnColumnHeader = function(grid, iColumn, text) {
    var thd = jQuery("thead:first", grid[0].grid.hDiv)[0];
    jQuery("tr.ui-jqgrid-labels th:eq(" + iColumn + ")", thd).attr("title", text);
  };

  // agrega titulo a la cabecera
  setTooltipsOnColumnHeader($("#dataGrid"), 1, "CPU - [seg.]");
  setTooltipsOnColumnHeader($("#dataGrid"), 2, "Lectura en Memoria - 5 MB / [seg.]");
  setTooltipsOnColumnHeader($("#dataGrid"), 3, "Escritura en Memoria - 5 MB / [seg.]");
  setTooltipsOnColumnHeader($("#dataGrid"), 4, "Lectura en Disco - 5 MB / [seg.]");
  setTooltipsOnColumnHeader($("#dataGrid"), 5, "Escritura en Disco - 5 MB / [seg.]");
  setTooltipsOnColumnHeader($("#dataGrid"), 6, "Ancho de Banda - 2 MB / [seg.]");
  setTooltipsOnColumnHeader($("#dataGrid"), 7, "Latencia - [seg.]");
  setTooltipsOnColumnHeader($("#dataGrid"), 8, "Procesamiento - [seg.]");
  setTooltipsOnColumnHeader($("#dataGrid"), 9, "Fecha de captura");
  setTooltipsOnColumnHeader($("#dataGrid"), 10, "Servidor de captura");

  // refrescar tabla
  $("#dataGrid").jqGrid('setGridParam', {
    datatype : 'json',
    url : jsonUrl
  }).trigger('reloadGrid');
}
