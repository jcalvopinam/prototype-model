package com.jcalvopinam.client.service;

import java.util.List;

import com.jcalvopinam.client.dto.Atributo;
import com.jcalvopinam.client.dto.Proveedor;
import com.jcalvopinam.client.dto.UltimaFechaEjecucion;
import com.jcalvopinam.client.model.ResultadoEjecucion;

/**
 * @author Juan Calvopina Morillo <juan.calvopina@gmail.com>
 *
 */
public interface IResultadoEjecucionService {

    public void save(ResultadoEjecucion resultado);

    public void update(ResultadoEjecucion resultado);

    public void delete(int id);

    public ResultadoEjecucion getResultadoEjecucion(int id);

    public List<ResultadoEjecucion> getAllResultadosEjecucion();

    public List<ResultadoEjecucion> getAllResultadosEjecucion(String serverName);

    public List<Proveedor> getUltimaEjecucion();

    public List<Proveedor> getEjecucionPromedio();

    public UltimaFechaEjecucion getUltimaFechaEjecucion();

    public List<ResultadoEjecucion> getComparacion();

    public List<ResultadoEjecucion> getPromedioComparacion();

    public List<Atributo> getAtributoByName(String atributo);

    public List<Atributo> getAtributoPromedioByName(String atributo);

}
